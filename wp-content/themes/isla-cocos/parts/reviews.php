<?php

$testimonials = [];

$query = new WP_Query([
    'post_type' => 'testimonial',
    'nopaging' => true
]);

while ($query->have_posts()) {
    $query->the_post();

    $author = get_the_title();
    $location = null;

    if (strpos($author, ' :: ') !== false) {
        $details = explode(' :: ', $author, 2);
        $author = trim($details[0]);
        $location = trim($details[1]);
    }

    $testimonials[] = [
        'author' => $author,
        'location' => $location,
        'content' => trim(strip_tags(get_the_content()))
    ];
}

wp_reset_postdata();

?>
<section class="section reviews" vesper-vcenter-root id="reviews">
    <div class="container" vesper-vcenter>

        <i class="reviews__icon"></i>
        <h2>
            <?= __('Reviews From Our Clients', 'g') ?>
        </h2>
        <p class="subheading">
            <?= __('We want to listen your valuable review.', 'g') ?>
        </p>

        <vesper-slider>
            <vesper-slides-container>

                <?php foreach ($testimonials as $testimonial) : ?>
                <vesper-slide>
                    <p>
                        <?= $testimonial['content']; ?>
                    </p>
                    <p class="review-author">
                        <?= $testimonial['author']; ?>
                        <?php if ($testimonial['location']) : ?>
                        &ndash; <span class="review-author__location"><?= $testimonial['location'] ?></span>
                        <?php endif; ?>
                    </p>
                </vesper-slide>
                <?php endforeach; ?>

            </vesper-slides-container>
        </vesper-slider>

    </div>
</section>
