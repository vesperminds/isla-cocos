<?php

$dishes = [];

$dish_types = get_terms('dish_type', [
    'hide_empty' => true
]);

foreach ($dish_types as $dish_type) {
    $dishes[] = [
        'id' => $dish_type->term_id,
        'title' => $dish_type->name,
        'description' => $dish_type->description,
        'dishes' => []
    ];
}

for ($i = 0; $i < count($dishes); $i++) {
    $query = new WP_Query([
        'post_type' => 'dish',
        'nopaging' => true,
        'tax_query' => [
            [
                'taxonomy' => 'dish_type',
                'field' => 'term_id',
                'terms' => $dishes[$i]['id']
            ]
        ],
        'orderby' => 'title',
        'order' => 'asc'
    ]);

    while ($query->have_posts()) {
        $query->the_post();

        $thumbnail_id = get_post_thumbnail_id();
        $image = null;

        if ($thumbnail_id) {
            $image = wp_get_attachment_image_src($thumbnail_id, 'medium');
        }

        $dishes[$i]['dishes'][] = [
            'title' => get_the_title(),
            'image' => $image ? $image[0] : null,
            'description' => trim(strip_tags(get_the_content()))
        ];
    }
}

wp_reset_postdata();

?>
<section class="section menu" vesper-vcenter-root id="menu">
    <div class="container" vesper-vcenter>
        <gugas-menu>

            <div class="menu-image"></div>

            <a href="#" gugas-menu-action="left"></a>
            <a href="#" gugas-menu-action="right"></a>

            <gugas-dish-type-container>

                <?php foreach ($dishes as $dish_type) : ?>
                <gugas-dish-type>
                    <h2><?= $dish_type['title'] ?></h2>
                    <!-- <p class="subheading"><?= $dish_type['description'] ?></p> -->
                    <div class="subheading-division"></div>
                    <vesper-slider slides-per-page="3" class="slider-desktop">

                        <vesper-slides-container>
                            <?php foreach ($dish_type['dishes'] as $dish): ?>
                            <vesper-slide>
                                <gugas-dish-photo style="background-image: url('<?= $dish['image'] ?>');"></gugas-dish-photo>
                                <h3><?= $dish['title'] ?></h3>
                                <gugas-dish-description>
                                    <?= $dish['description'] ?>
                                </gugas-dish-description>
                            </vesper-slide>
                            <?php endforeach; ?>
                        </vesper-slides-container>

                    </vesper-slider>

                    <vesper-slider slides-per-page="1" class="slider-mobile">

                        <vesper-slides-container>
                            <?php foreach ($dish_type['dishes'] as $dish): ?>
                            <vesper-slide>
                                <gugas-dish-photo style="background-image: url('<?= $dish['image'] ?>');"></gugas-dish-photo>
                                <h3><?= $dish['title'] ?></h3>
                                <gugas-dish-description>
                                    <?= $dish['description'] ?>
                                </gugas-dish-description>
                            </vesper-slide>
                            <?php endforeach; ?>
                        </vesper-slides-container>

                    </vesper-slider>

                </gugas-dish-type>
                <?php endforeach; ?>

            </gugas-dish-type-container>

        </gugas-menu>
        <div class="note">
            <?= __('Menu images are for illustrative purposes only.', 'g') ?>
        </div>
    </div>
</section>
