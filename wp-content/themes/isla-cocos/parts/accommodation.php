<section class="section section__bg-img accommodation" vesper-vcenter-root id="accommodation">
    <div class="container" vesper-vcenter>

        <h2>
            <?= __('Accommodation', 'g') ?>
        </h2>
        <p>
            <?= __('Stay with us and will assist your special requests. We take great pride in offering guests the highest level of service and our unfailing hospitality.', 'g') ?>
        </p>

    </div>
</section>
