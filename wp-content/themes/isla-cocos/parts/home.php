<section class="section section-first section__bg-neg section__bg-video home" vesper-vcenter-root id="home">
    <div class="section__bg-video--bg">
        <video
        vesper-video-src="<?= theme_url('/vid/shrimps.mp4') ?>"
        vesper-video-poster="<?= theme_url('/vid/shrimps.jpg') ?>"></video>
    </div>
    <div class="container" vesper-vcenter>

        <h2 class="main-heading">
            <?= str_replace('[br]', '<br class="br--no-mobile">', __('For us, it’s all[br]about good food', 'g')) ?>
        </h2>
        <p class="subheading">
            <?= __('Experience the best cuisine', 'g') ?>
        </p>

        <div class="temporary-banner">
            <?= __('Due to vacations the restaurant will be closed between June 1st and July 8th, 2016.', 'g') ?>
        </div>

        <div class="scroll-indicator">
            <p><?= __('Scroll to learn more', 'g') ?></p>
            <p><i class="scroll-indicator__arrow"></i></p>
        </div>
    </div>
</section>
