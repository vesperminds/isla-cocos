<section class="section section__bg-neg about-us" vesper-vcenter-root id="about-us">
    <div class="container" vesper-vcenter>

        <h2>
            <?= __('About Us', 'g') ?>
        </h2>
        <p>
            <?= __('Opened since 2002.', 'g') ?>
        </p>
        <p>
            <?= __('Visit us and enjoy a unique experience.', 'g') ?>
        </p>
        <p>
            <?= __('We want to offer you the best from our menu with particular taste, mixing the best from two worlds: porteño flavours with the culinary culture from Germany.', 'g') ?>
        </p>
        <p>
            <?= __('It\'s pretty easy to find us in town, one block north the cruise ship terminal.', 'g') ?>
        </p>

    </div>
</section>
