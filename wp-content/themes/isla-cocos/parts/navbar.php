<?php $langList = include(__DIR__ . DIRECTORY_SEPARATOR . '_langlist.php'); ?>
<nav class="navbar home-navbar">
    <ul>
        <li class="logo">
            <i></i>
        </li>
        <li>
            <a href="#home"><?= __('Home', 'g') ?></a>
        </li>
        <li>
            <a href="#about-us"><?= __('About', 'g') ?></a>
        </li>
        <li>
            <a href="#menu"><?= __('Menu', 'g') ?></a>
        </li>
        <li>
            <a href="#accommodation"><?= __('Accommodation', 'g') ?></a>
        </li>
        <li>
            <a href="#reviews"><?= __('Reviews', 'g') ?></a>
        </li>
        <li>
            <a href="#contact-us"><?= __('Contact Us', 'g') ?></a>
        </li>
        <li class="icon facebook">
            <a target="_blank" href="https://www.facebook.com/RESTAURANTE-GUGAS-182630778451547/"><i>Facebook</i></a>
        </li>
        <?php foreach ($langList as $lang): ?>
        <li class="lang">
            <a href="<?= $lang['url'] ?>"><?= $lang['abbr'] ?></a>
        </li>
        <?php endforeach; ?>
    </ul>
</nav>
