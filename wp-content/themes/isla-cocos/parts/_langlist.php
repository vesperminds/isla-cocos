<?php
$languages = icl_get_languages();
$langList = [];

if (!empty($languages)) {
    foreach ($languages as $language) {
        if (!empty($language['active'])) {
            continue;
        }

        $langList[] = [
            'abbr' => $language['language_code'],
            'name' => $language['native_name'],
            'url' => $language['url']
        ];
    }
}

return $langList;
