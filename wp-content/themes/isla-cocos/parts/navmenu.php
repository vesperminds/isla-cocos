<?php $langList = include(__DIR__ . DIRECTORY_SEPARATOR . '_langlist.php'); ?>
<?php foreach ($langList as $lang): ?>
<div class="navmenu--languages">
    <a href="<?= $lang['url'] ?>"><?= $lang['abbr'] ?></a>
</div>
<?php endforeach; ?>
<a href="#" class="navmenu--button"></a>
<nav class="navmenu">
    <div class="logo"></div>
    <ul>
        <li>
            <a href="#home"><?= __('Home', 'g') ?></a>
        </li>
        <li>
            <a href="#about-us"><?= __('About', 'g') ?></a>
        </li>
        <li>
            <a href="#menu"><?= __('Menu', 'g') ?></a>
        </li>
        <li>
            <a href="#accommodation"><?= __('Accommodation', 'g') ?></a>
        </li>
        <li>
            <a href="#reviews"><?= __('Reviews', 'g') ?></a>
        </li>
        <li>
            <a href="#contact-us"><?= __('Contact Us', 'g') ?></a>
        </li>
    </ul>
    <div class="icon-wrapper">
        <span class="icon facebook">
            <a target="_blank" href="https://www.facebook.com/RESTAURANTE-GUGAS-182630778451547/"><i>Facebook</i></a>
        </span>
    </div>
</nav>
