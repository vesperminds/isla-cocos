<section class="section section__bg-neg contact-us" ng-controller="contactUsCtrl" vesper-vcenter-root id="contact-us">
    <div class="container" vesper-vcenter>

        <h2>
            <?= __('Contact Us', 'g') ?>
        </h2>
        <p class="subheading">
            <?= __('We are here to listen you.', 'g') ?>
        </p>

        <p>
            <?= __('We are open from <strong>Tuesday</strong> to <strong>Sunday</strong>, from <strong>11:00am</strong>.', 'g') ?>
        </p>

        <p>
            <?= __('Phone number: <a href="tel:+50626610707">(506) 2661-0707</a>', 'g') ?>
        </p>
        <div class="contact-thanks" ng-show="sent">
            <?= __('Thanks for contacting us. We\'ll get back to you as soon as possible') ?>
        </div>

        <form ng-hide="sent" action="" method="post" name="contactForm" novalidate>
            <input type="hidden" name="lang" ng-model="contact.lang" value="<?= __('English') ?>">

            <div class="gr-row">
                <div class="gr-6 gr-12@mobile form--field">
                    <input type="text" name="fullname" ng-model="contact.fullname" required placeholder="<?= __('Enter your name here', 'g') ?>" />
                </div>
            </div>

            <div class="gr-row">
                <div class="gr-6 gr-12@mobile form--field">
                    <input type="email" name="email" ng-model="contact.email" required placeholder="<?= __('Enter your email address here', 'g') ?>" />
                </div>
            </div>

            <div class="gr-row">
                <div class="gr-6 gr-12@mobile form--field">
                    <select name="topic" vesper-select ng-model="contact.topic" required placeholder="<?= __('Select from the following topics', 'g') ?>">
                        <option value="Reservations"><?= __('Reservations', 'g') ?></option>
                        <option value="Comments"><?= __('Comments', 'g') ?></option>
                        <option value="Reviews"><?= __('Reviews', 'g') ?></option>
                    </select>
                </div>
            </div>

            <div class="gr-row">
                <div class="gr-6 gr-12@mobile form--field">
                    <textarea name="message" ng-model="contact.message" rows="5" required placeholder="<?= __('Enter your message here', 'g') ?>"></textarea>
                </div>
            </div>

            <div class="gr-row">
                <div class="gr-12 form--field form--field-centered">
                    <button type="submit" ng-click="send($event, contactForm)" ng-disabled="contactForm.$invalid || APIRequest"><?= __('Submit', 'g') ?></button>
                </div>
            </div>

            <div class="gr-row">
                <div class="gr-12 small-note">
                    <?= __('You can pay for all orders using the following cards: Visa, American Express and Mastercard.', 'g') ?>
                </div>
            </div>

        </form>

    </div>
</section>
