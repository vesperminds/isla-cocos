<?php

define('DEBUG_MODE', strpos($_SERVER['SERVER_NAME'], '0.0.0.0') !== false);

add_action('wp_enqueue_scripts', function () {
    // CSS
    wp_enqueue_style(
        'css-main',
        get_stylesheet_directory_uri() . '/dist/css/main.css',
        [],
        (string) filemtime(get_stylesheet_directory() . '/dist/css/main.css')
    );

    // JS

    wp_deregister_script('jquery');

    wp_register_script('jquery', 'https://code.jquery.com/jquery-2.2.0.min.js', [], false, true);

    wp_register_script('angular', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js', [
        'jquery'
    ], false, true);

    wp_enqueue_script('js-main', get_stylesheet_directory_uri() . '/dist/js/main.js', [
        'jquery',
        'angular'
    ], (string) filemtime(get_stylesheet_directory() . '/dist/js/main.js'), true);
});

function theme_url($url)
{
    return get_stylesheet_directory_uri() . $url;
}

/* JSON */

function tt_json_output($data) {

    @header('Content-Type: application/json; charset=utf-8');
    echo json_encode($data);

}

/* Form (Contact) */

define('EMAIL_EOL', "\r\n");

$tt_form_handlers = [

    'contact' => function($step, $data) {

        if ($step == 'validate') {

            $errors = [];

            if (empty($data['fullname'])) {
                $errors[] = 'fullname';
            }

            if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = 'email';
            }

            if (empty($data['topic'])) {
                $errors[] = 'topic';
            }

            if (empty($data['message'])) {
                $errors[] = 'message';
            }

            return $errors;

        }
        elseif ($step == 'compose') {

            $message = [];

            $message[] = 'Contact Request';
            $message[] = '---------------';
            $message[] = '';
            $message[] = 'Full Name: ' . strip_tags($data['fullname']);
            $message[] = 'Email: ' . strip_tags($data['email']);
            $message[] = 'Language: ' . strip_tags($data['lang']);
            $message[] = 'Topic: ' . strip_tags($data['topic']);
            $message[] = 'Message:';
            $message[] = strip_tags($data['message']);

            return $message;

        }
        elseif ($step == 'subject') {
            return 'Contact Form';
        }
        elseif ($step == 'compose-user') {

            $message = [];

            $message[] = __('Hey ', 'g') . $data['fullname'] . ',';
            $message[] = '';
            $message[] = __('Thank you for getting in touch with us. We\'ll reply you as soon as possible.', 'g');
            $message[] = '';
            $message[] = __('Thanks,', 'g');
            $message[] = __('- Gugas team', 'g');


            return $message;

        }
        elseif ($step == 'subject-user') {
            return __('Thank you for contacting us');
        }

        return false;

    },
];

function tt_handle_form() {

    global $tt_form_handlers;

    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        return;
    }

    if (empty($_POST['_fh']) || empty($tt_form_handlers[$_POST['_fh']])) {
        return;
    }

    $fh = $_POST['_fh'];

    $data = $_POST;

    foreach ($data as $key => $value) {
        $data[$key] = trim(strip_tags($value));
    }

    $validationErrors = $tt_form_handlers[$fh]('validate', $data);

    if (!empty($validationErrors)) {
        tt_json_output([
            'done' => false,
            'error' => 'E_INVALID_FIELDS',
            'message' => 'One or more fields are not valid',
            'details' => $validationErrors
        ]);
        exit;
    }

    $admin_email = get_option('admin_email');
    $admin_rcpt = [ $admin_email ];

    if (!DEBUG_MODE) {
        $admin_rcpt[] = 'vesperminds@gmail.com';
    }

    $subject = $tt_form_handlers[$fh]('subject', $data);
    $message = $tt_form_handlers[$fh]('compose', $data);

    if (!DEBUG_MODE) {
        wp_mail($admin_rcpt, $subject, implode(EMAIL_EOL, $message));
    }

    $subjectUser = $tt_form_handlers[$fh]('subject-user', $data);
    $messageUser = $tt_form_handlers[$fh]('compose-user', $data);

    if (!DEBUG_MODE) {
        wp_mail($data['email'], $subjectUser, implode(EMAIL_EOL, $messageUser));
    }

    tt_json_output([
        'done' => true
    ]);
    exit;

}
