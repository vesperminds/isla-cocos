<!DOCTYPE html>
<html ng-app="app" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Gugas Restaurant &amp; Marisquería &ndash; Puntarenas, Costa Rica</title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <?php if (isset($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'], 'gugasgourmet.com') !== false): ?>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-75147799-1', 'auto');
    ga('send', 'pageview');
    </script>
    <?php endif; ?>
</head>
<body>
