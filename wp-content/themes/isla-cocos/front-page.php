<?php tt_handle_form(); ?>

<?php get_template_part('partials/head'); ?>

<?php get_template_part('parts/navbar'); ?>

<?php get_template_part('parts/home'); ?>
<?php get_template_part('parts/about-us'); ?>
<?php get_template_part('parts/menu'); ?>
<?php get_template_part('parts/accommodation'); ?>
<?php get_template_part('parts/reviews'); ?>
<?php get_template_part('parts/contact-us'); ?>

<?php get_template_part('parts/navmenu'); ?>

<?php get_template_part('partials/tail'); ?>
