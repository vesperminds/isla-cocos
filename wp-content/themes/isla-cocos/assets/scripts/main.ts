/// <reference path="./typings/tsd.d.ts" />

class AnimatedScroll {

    public static to(dest: JQuery, delay = 1000) {
        $('html, body')
        .animate({
            scrollTop: Math.ceil(dest.offset().top - AnimatedScroll.calcOffset())
        }, delay);
    };

    private static offsets: string[] = [];

    public static addOffset(selector: string) {
        if (AnimatedScroll.offsets.indexOf(selector) > -1) return;
        AnimatedScroll.offsets.push(selector);
    }

    public static calcOffset(): number {
        let offset: number = 0;

        AnimatedScroll.offsets.forEach(el => {
            let $el = $(el)

            if (!$el.length) return;

            offset += $el.outerHeight();
        });

        return offset;
    }

}

// Init
$(() => {

    AnimatedScroll.addOffset('.home-navbar');

    if ($('#wpadminbar').length) {
        $('nav.navbar').css({ top: 32 });
    }

});

// Tmp
$(() => {

    let banner: JQuery = $('.temporary-banner');
    if (!banner.length) return;

    setTimeout(() => {
      banner.addClass('temporary-banner--animate');
    }, 2000);

});

// Navigation
$(() => {

    let nav: JQuery = $('nav');
    if (!nav.length) return;

    $('body').on('click', function() {
        let $body: JQuery = $('body');

        if ($body.hasClass('navmenu--open'))
            $body.removeClass('navmenu--open');
    });

    nav.on('click', function(ev: JQueryEventObject) {
        ev.stopPropagation();
    });

    $('.navmenu--button').on('click', function(ev: JQueryEventObject) {

        ev.preventDefault();
        ev.stopPropagation();

        $('body').toggleClass('navmenu--open');

    });

    nav.find('a').each(function(index: number, el: Element) {

        let $el: JQuery = $(el),
            href: string = $el.attr('href');

        if (!href || href.indexOf('#') !== 0) return;
        if (!$(href).length) return;

        $el.on('click', function(ev: JQueryEventObject) {

            ev.preventDefault();
            ev.stopPropagation();

            let href: string = $(this).attr('href'),
                dest: JQuery = $(href);

            AnimatedScroll.to(dest);

            $('body').removeClass('navmenu--open');

        });

    });

});

// Angular

var app = angular.module('app', []);

// Misc

class UniqueID {

    private static track: number = 1000;

    public static generate(prefix?: string): string {
        UniqueID.track++;
        return `${prefix ? prefix + '-' : ''}${UniqueID.track}`;
    }

}

// Video

interface BackgroundVideoAttrs {
    src: string;
    poster: string;
}

interface BackgroundVideoElement extends BackgroundVideoAttrs {
    el: JQuery;
    loaded: boolean;
}

class BackgroundVideo {

    public static elements: BackgroundVideoElement[] = [];

    public static attach(el: JQuery, attrs: BackgroundVideoAttrs) {

        if (el.data('BackgroundVideo__attached'))
            return;

        el.data('BackgroundVideo__attached', true);

        BackgroundVideo.elements.push({
            el,
            src: attrs.src,
            poster: attrs.poster,
            loaded: false
        });

    }

    public static enable(index: number) {

        let { el, src, poster, loaded } = BackgroundVideo.elements[index];

        if (loaded)
            return;

        el.attr({
            src,
            poster
        });

        let domEl = <HTMLVideoElement>el[0];

        domEl.load();
        domEl.muted = true;
        domEl.loop = true;
        domEl.play();

        BackgroundVideo.elements[index].loaded = true;

    }

    public static calculate() {

        BackgroundVideo.elements.forEach((item, index) => {
            if (item.el.is(':visible'))
                BackgroundVideo.enable(index);
        });

    }

}

$(() => {
    $(window).on('resize', () => { BackgroundVideo.calculate() });
    BackgroundVideo.calculate();
});

app.directive('vesperVideoSrc', () => {

    var link = (scope: angular.IScope, el: JQuery, attrs: any) => {

        BackgroundVideo.attach(el, {
            src: attrs.vesperVideoSrc || '',
            poster: attrs.vesperVideoPoster || ''
        });

    };

    return {
        restrict: 'A',
        link: link
    };

});

// Slider

app.directive('vesperSlider', () => {

    var link = (scope: angular.IScope, el: JQuery, attrs: any) => {

        let container = el.find('vesper-slides-container:first'),
            slides = container.find('vesper-slide'),
            spp = parseInt(attrs.slidesPerPage || 1, 10);

        if (!el.attr('id'))
            el.attr('id', UniqueID.generate('vesper-slider'));

        let id = el.attr('id');

        if (!slides.length)
            return;

        if (attrs.adjustSlides) {
            if (spp > slides.length)
                spp = slides.length;
        }

        container.css({
            width: `${slides.length * (100 / spp)}%`
        });

        let controls = $('<vesper-slider-controls></vesper-slider-controls>');
        controls.insertBefore(container);

        slides.each((index, el) => {
            let $el = angular.element(el);

            $el.css({
                width: `${100 / slides.length}%`
            });
        });

        for (let index = 0; index < Math.ceil(slides.length / spp); index++) {

            let control = $('<input/>'),
                selector = $('<label></label>'),
                controlId = `${id}-slide-control-${index + 1}`;

            control.attr({
                type: 'radio',
                name: `${id}-slide-control`,
                id: controlId
            });
            control.addClass('slider--slide-control');
            if (index === 0)
                control.prop('checked', true);
            control.data('slider-index', index);
            control.appendTo(controls);

            selector.attr({
                for: controlId,
                id: `${id}-slide-selector`
            });
            selector.addClass('slider--slide-selector');
            selector.appendTo(controls);

        }

        el.on('change click', '.slider--slide-control', function() {
            let index = $(this).data('slider-index');

            container.css({
                marginLeft: `-${index * 100}%`
            });
        });

    };

    return {
        restrict: 'E',
        link: link
    };

});

// Select Box styling

app.directive('vesperSelect', () => {

    var link = (scope: angular.IScope, el: JQuery, attrs: any) => {

        let placeholder = el.attr('placeholder') || '';

        el
        .attr('vesper-select-control', '1')
        .wrap('<vesper-select-wrapper></vesper-select-wrapper>');

        let label = $('<span></span>');
        label.insertBefore(el);

        if (attrs.ngModel) {
            scope.$watch(attrs.ngModel, (value: string) => {
                label.html(value || placeholder || '&nbsp;');
            });
        }

    };

    return {
        restrict: 'A',
        link: link
    };
});

// Contact Form

interface ContactUsContactData {
    fullname: string;
    email: string;
    topic: string;
    message: string;
    lang: string;
    _fh: string;
}

interface ContactUsCtrlScope extends angular.IScope {
    APIRequest: boolean;
    sent: boolean;
    contact: ContactUsContactData;
    contactForm: angular.IFormController;
    reset(): void;
    send($event: angular.IAngularEvent, form: angular.IFormController): void;
}

app.controller('contactUsCtrl', ['$scope', '$http', ($scope: ContactUsCtrlScope, $http: angular.IHttpService) => {

    $scope.APIRequest = false;

    $scope.contact = {
        fullname: '',
        email: '',
        topic: '',
        message: '',
        lang: '',
        _fh: 'contact'
    }

    $scope.sent = false;

    $scope.reset = () => {
        for (let k in $scope.contact) {
            if (!$scope.contact.hasOwnProperty(k)) continue;
            if (k == 'lang') continue;
            if (k == '_fh') continue;
            (<any>$scope.contact)[k] = '';
        }

        $scope.contactForm.$setUntouched();
    };

    $scope.send = ($event: angular.IAngularEvent) => {
        $event.preventDefault();

        if ($scope.contactForm.$invalid)
            return;

        $scope.APIRequest = true;

        $http({
            method: 'post',
            url: window.location.href,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param($scope.contact)
        }).then((response: any) => {
            $scope.sent = true;
        }, (err: any) => {
            console.error(err);
        }).finally(() => {
            $scope.APIRequest = false;
            $scope.reset();
        });
    };

}]);


// Menu (like a slider, but different)

app.directive('gugasMenu', () => {

    var link = (scope: angular.IScope, el: JQuery, attrs: any) => {

        let container = el.find('gugas-dish-type-container:first'),
            slides = container.find('gugas-dish-type');

        if (!el.attr('id'))
            el.attr('id', UniqueID.generate('gugas-menu'));

        let id = el.attr('id');

        if (!slides.length)
            return;

        let controlLeft = el.find(''),
            controlRight = el.find('[gugas-menu-action="right"]');

        container.css({
            width: `${slides.length * 100}%`
        });

        slides.each((index, el) => {
            let $el = angular.element(el);

            $el.css({
                width: `${100 / slides.length}%`
            });

        });

        el.on('click', '[gugas-menu-action="left"]', function(ev) {
            ev.preventDefault();

            let index = parseInt(el.data('menu-index') || 0, 10) - 1;

            if (index < 0) {
                index = slides.length - 1;
            }

            el.data('menu-index', index);

            container.css({
                marginLeft: `-${index * 100}%`
            });
        });

        el.on('click', '[gugas-menu-action="right"]', function(ev) {
            ev.preventDefault();

            let index = parseInt(el.data('menu-index') || 0, 10) + 1;

            if (index > slides.length - 1) {
                index = 0;
            }

            el.data('menu-index', index);

            container.css({
                marginLeft: `-${index * 100}%`
            });
        });

    };

    return {
        restrict: 'E',
        link: link
    };

});
