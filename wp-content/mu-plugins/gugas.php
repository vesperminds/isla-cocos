<?php defined('ABSPATH') or die;
/*
Plugin Name: Gugas
Description: Gugas Wordpress' Backend
Author: VesperMinds
Author URI: http://www.vesperminds.com/
*/

// Supports

add_action('after_setup_theme', function () {
    add_theme_support('post-thumbnails', [
        'post',
        'page',
        'dish'
    ]);
    add_theme_support('menus');
});

// Dishes

add_action('init', function () {
    // `dish` Post Type
    register_post_type('dish', [
       'labels' => [
           'name' => 'Platos',
           'singular_name' => 'Plato'
       ],
       'public' => true,
       'menu_position' => 5,
       'menu_icon' => 'dashicons-carrot',
       'capability_type' => 'page',
       'supports' => [
           'title',
           'editor',
           'thumbnail'
       ]
    ]);

    // `dish_type` Taxonomy
    register_taxonomy('dish_type', 'dish', [
        'labels' => [
            'name' => 'Tipos de Plato',
            'singular_name' => 'Tipo de Plato'
        ],
        'hierarchical' => true
    ]);

    // Map taxonomy to type
    register_taxonomy_for_object_type('dish_type', 'dish');
});

// Testimonials

add_action('init', function () {
    // `testimonial` Post Type
    register_post_type('testimonial', [
       'labels' => [
           'name' => 'Testimonios',
           'singular_name' => 'Testimonio'
       ],
       'public' => true,
       'menu_position' => 5,
       'menu_icon' => 'dashicons-megaphone',
       'capability_type' => 'page',
       'rewrite' => false
    ]);
});

/** changing default wordpres email settings */

add_filter('wp_mail_from', function($old = '') {
	return 'no-reply@gugasgourmet.com';
});

add_filter('wp_mail_from_name', function($old = '') {
	return 'Gugas';
});
