# Isla Cocos

## Preparación

- Clonar este repositorio
- Ejecutar `npm install`
- Descomprimir Wordpress en la carpeta raíz
- Instalar Wordpress
- Activar tema **Isla Cocos**

## Desarrollo

- Ejecutar `gulp`
